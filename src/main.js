var core_1 = require('angular2/core');
var browser_1 = require('angular2/platform/browser');
var router_1 = require('angular2/router');
var app_1 = require('components/app/app');
var providers_1 = require('modules/auth/providers');
var providers_2 = require('modules/task/providers');
require('./styles/styles.scss');
Firebase.INTERNAL.forceWebSockets();
browser_1.bootstrap(app_1.App, [
    router_1.ROUTER_PROVIDERS,
    providers_1.AUTH_PROVIDERS,
    providers_2.TASK_PROVIDERS,
    core_1.provide(router_1.APP_BASE_HREF, { useValue: '/' })
]).catch(function (error) { return console.error(error); });
//# sourceMappingURL=main.js.map