export const FIREBASE_URL: string = 'https://ralfger.firebaseio.com/';
export const FIREBASE_TASKS_URL: string = `${FIREBASE_URL}/tasks`;
