import { AuthService } from 'modules/auth/auth-service';
export interface ITask {
  completed: boolean;
  createdAt: number;
  key?: string;
  title: string;
}


export class Task implements ITask {
  completed: boolean = false;
  user: string;
  createdAt: number = Firebase.ServerValue.TIMESTAMP;
  title: string;

  constructor(title: string, private auth:AuthService) {
    this.title = title;
    this.user = auth.nameUser
  }
}
