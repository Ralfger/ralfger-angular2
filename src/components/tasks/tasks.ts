import { Component } from 'angular2/core';
import { CanActivate } from 'angular2/router';
import { AuthRouteHelper } from 'modules/auth/auth-route-helper';
import { TaskStore } from 'modules/task/task-store';
import { TaskForm } from './task-form/task-form';
import { TaskList } from './task-list/task-list';
import { ROUTER_DIRECTIVES } from 'angular2/router';

const template: string = require('./tasks.html');


@Component({
  directives: [
    TaskForm,
    TaskList,
    ROUTER_DIRECTIVES
  ],
  selector: 'tasks',
  template
})

@CanActivate(() => AuthRouteHelper.requireAuth())

export class Tasks {
  constructor(public taskStore: TaskStore) {}
}
