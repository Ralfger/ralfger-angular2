import { Component } from 'angular2/core';
import { CanActivate } from 'angular2/router';
import { ROUTER_DIRECTIVES } from 'angular2/router';

const styles: string = require('./home.scss');
const template: string = require('./home.html');


@Component({
  directives: [ROUTER_DIRECTIVES],
  selector: 'home',
  styles: [styles],
  template
})


export class Home {
}
