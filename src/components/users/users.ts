import { ChangeDetectionStrategy, Component, View, Input, NgZone } from 'angular2/core';
import { ROUTER_DIRECTIVES } from 'angular2/router';
import { FIREBASE_URL } from '../../config';
import { ReplaySubject } from 'rxjs/subject/ReplaySubject';
import { List } from 'immutable';
import { AuthService } from 'modules/auth/auth-service';


const styles:string = require('./users.scss');
const template:string = require('./users.html');


@Component({
    selector: 'users',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template,
    directives: [ROUTER_DIRECTIVES]

})


export class Users {
    @Input() names: string[];

    constructor(private auth:AuthService, private _ngZone:NgZone) {
        this.names = [''];


    }

    cos() {
        let ref = new Firebase(`${FIREBASE_URL}`);
        if (this.auth.nameUser) {
            ref.set({
                name: this.auth.nameUser
            }, function (error) {
            });
        }
        ref.once("value", function (snapshot) {
            let nameUser = snapshot.val().name;
            console.log(nameUser);
            this.names.push(nameUser);
        });
        this._ngZone.runOutsideAngular(() => {
            this._ngZone.run(() => {
                console.log('Outside Done!');
                this.names.push('dsf');
            });
        });
    }
}
